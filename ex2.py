import email
import imaplib
import os
import sys
import ex1


def download_attachment(username, password):
    detach_dir = ''
    m = imaplib.IMAP4_SSL("imap.gmail.com")
    m.login('lmntexelimar@gmail.com', 'luminatePass1')
    m.select("INBOX")

    resp, items = m.search(None, '(SUBJECT "Singular Python Exercise")')
    items = items[0].split()

    for emailid in items:
        resp, data = m.fetch(emailid, "(RFC822)")
        email_body = data[0][1]
        mail = email.message_from_bytes(email_body)
        temp = m.store(emailid, '+FLAGS', '\\Seen')
        m.expunge()

        if mail.get_content_maintype() != 'multipart':
            continue

        for part in mail.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue

            filename = part.get_filename()
            att_path = os.path.join(detach_dir, filename)

            if not os.path.isfile(att_path):
                fp = open(att_path, 'wb')
                fp.write(part.get_payload(decode=True))
                fp.close()

            return filename


def main(username, password):
    file_path = download_attachment(username, password)
    ex1.main(file_path)


if __name__ == '__main__':
    args = sys.argv
    username = args[1]
    password = args[2]
    main(username, password)


