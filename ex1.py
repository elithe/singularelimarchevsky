import sys
import pandas as pd

HEADER_ROW_NUM = 3


def summerise_installs_and_cost_by_date(date_group):
    group_date = date_group.iloc[0]["Date"]
    date_str = group_date.strftime("%Y-%m-%d")
    total_installs = str(sum(date_group["Installs"]))
    total_cost = str(sum(date_group["Cost"]))
    msg = "Total installs: {}, Total cost: {} for date: {}".format(total_installs, total_cost, date_str)
    return msg

def summerise_data_per_platform_and_app(platform_app_group):
    group_platform = platform_app_group.iloc[0]["Platform"]
    group_app = platform_app_group.iloc[0]["App"]
    total_installs = str(sum(platform_app_group["Installs"]))
    total_cost = str(sum(platform_app_group["Cost"]))
    msg = "Total installs: {}, Total cost: {} for platform: {} and app: {}".format(total_installs, total_cost, group_platform, group_app)
    return msg


def main(file_path):
    excel_df = pd.read_excel(file_path, header=HEADER_ROW_NUM)
    total_cost = excel_df.iloc[len(excel_df)-1]["Cost"]
    total_installs = excel_df.iloc[len(excel_df)-1]["Installs"]
    print("Total installs: {}, Total cost: {} \n".format(total_installs, total_cost))

    # Drop the last line with totals so we can use a nice clean df for further calculations
    excel_df = excel_df.drop(excel_df.index[len(excel_df) - 1])
    results = excel_df.groupby(["Date"]).apply(summerise_installs_and_cost_by_date)
    for msg in results.unique():
        print(msg)
    print("\n")

    # Extract platform from the campaign name
    excel_df["Platform"] = excel_df["Campaign"].apply(lambda campaign: campaign.split(" ")[0])
    results = excel_df.groupby(["Platform", "App"]).apply(summerise_data_per_platform_and_app)
    for msg in results.unique():
        print(msg)


if __name__ == '__main__':
    args = sys.argv
    file_path = args[1]
    main(file_path)

