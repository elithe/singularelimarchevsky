
# Singular
Python script that analyzes an Excel report

## Getting Started
To run the tool you need to have latest python3.6 version installed.

example to run first part:
```
python3.6 ex1.py Junior Exercise - Report.xlsx
```

example to run second part:
```
python3.6 ex2.py elithe@gmail.com elis_secret_password
```

## Small design issue
I could print directly from the apply functions and not send back the messages but then we would have seen the first group printed twice.
As far as I know it is pandas design so that's why i printed it in the way I did it.
 
## Author

**Eli Marchevsky** - elithe@gmail.com
